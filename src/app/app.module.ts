import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadersComponent } from './components/headers/headers.component';
import { MesaggeListComponent } from './components/mesagge-list/mesagge-list.component';
import { MessageComponent } from './components/message/message.component';
import {_MessagesService} from '../app/service/_MessageService.service';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeadersComponent,
    MesaggeListComponent,
    MessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [_MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
