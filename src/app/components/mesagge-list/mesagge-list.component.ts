import { Component, OnInit } from '@angular/core';
import { _MessagesService} from '../../service/_MessageService.service'

@Component({
  selector: 'app-mesagge-list',
  templateUrl: './mesagge-list.component.html',
  styleUrls: ['./mesagge-list.component.scss']
})
export class MesaggeListComponent implements OnInit {
public list: string[] = [];
  constructor(_MessagesService: _MessagesService) {

    this.list = _MessagesService.getMessageList();
   }

  ngOnInit(): void {
  }

}
