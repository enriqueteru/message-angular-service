import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaggeListComponent } from './mesagge-list.component';

describe('MesaggeListComponent', () => {
  let component: MesaggeListComponent;
  let fixture: ComponentFixture<MesaggeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesaggeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaggeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
