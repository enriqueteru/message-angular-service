import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {_MessagesService} from '../../service/_MessageService.service'
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

messageForm: FormGroup;


  constructor( private _MessagesService : _MessagesService) {

    this.messageForm = new FormGroup({
      message: new FormControl('', Validators.minLength(2))
    })
   }



  ngOnInit(): void {
  }

  onSubmit(): void{
    this._MessagesService.pushNewMessage(this.messageForm.value.message);
    this.messageForm.reset();

      }

}
